<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\aset */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kode_aset')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Nama Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'nama_aset')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Serial Number</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'sn_aset')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Lokasi Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Keterangan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kondisi Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kondisi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
    </div>

	<div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
