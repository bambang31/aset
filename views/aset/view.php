<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\aset */

$this->title = 'Data Aset '.$model->kode_aset;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Aset'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-aset-view">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                    'kode_aset',
                    'nama_aset',
                    'sn_aset',
                    'lokasi',
                    'keterangan',
                    'kondisi',
                 ],
                ]) ?>
                <p>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Print'), ['gen-pdf', 'id' => $model->kode_aset], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Update'), ['update', 'id' => $model->kode_aset], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>

               <?= Html::a(Yii::t('app', '<i class="ico fa fa-remove"></i> Delete'), ['delete', 'id' => $model->kode_aset], [
                    'class' => 'btn btn-icon btn-icon-left btn-danger btn-xs waves-effect waves-light',
                    'data' => [
                        'confirm' => 'Anda yakin akan menghapus data aset? Seluruh data akan terhapus!',
                        'method' => 'POST',
                    ],
                ]) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
            </p>
            </div>
        </div>
    </div>

</div>
