<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\aset */
$this->title = Yii::t('app', 'Ubah Data Aset {nameAttribute}', [
    'nameAttribute' => $model->kode_aset,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Aset'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_aset, 'url' => ['view', 'id' => $model->kode_aset]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Ubah Data Aset');
?>

<div class="aset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
