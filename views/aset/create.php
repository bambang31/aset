<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\aset */

$this->title = Yii::t('app', 'Tambah Data Aset');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Aset'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-merk-create">
    <div class="col-lg-12">
		<div class="box-content card danger">
			<h4 class="box-title"><?= Html::encode($this->title) ?></h4>
			<div class="card-content">
				<?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>
			</div>
		</div>
	</div>
</div>

