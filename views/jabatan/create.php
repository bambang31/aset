<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\jabatan */

$this->title = Yii::t('app', 'Tambah Data Jabatan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-merk-create">
    <div class="col-lg-12">
		<div class="box-content card danger">
			<h4 class="box-title"><?= Html::encode($this->title) ?></h4>
			<div class="card-content">
				<?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>
			</div>
		</div>
	</div>
</div>

