<?php
// print_r($model);
// exit();

use yii\helpers\Html;
use yii\grid\GridView;
use yii\mPDF;
use app\models\karyawan;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AsetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Karyawan - Aset Admin');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- ini yang mau di print -->

<div class="panel panel-default panel-body">
    <div class="row tes2 mb-3 mt-3">
        <div class="col-xs-1 "></div>
            <div class="col-xs-10 text-center">
        <p class="m-0 p-0"><b class="text-dark" style="font-size: 18pt">IT TOWER CHEVRON</p>
        <p class="m-0 p-0"><b class="text-dark" style="font-size: 18pt">PT. SURYA KARSA MEDIAFORMASINDO</> </b>
        <br>
                                     Jln. Putri Tujuh Dumai Timur 
        <br>
                                <p><center><b>Laporan Karyawan</b></center></p>
        </div>
    </div>
</div>

<!-- ini tabel -->

<!DOCTYPE html>
<html>
<body>
    <table style="margin:20px auto;" border="1">
        <tr>
            <th>No</th>
            <th>Kode Aset</th>
            <th>Nama Aset</th>
            <th>Serial Number</th>
            <th>Kondisi</th>
        </tr>
        <?php 
        $no = 1;
        foreach($model as $aset){ 
        ?>
        <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $aset->kode_aset ?></td>
            <td><?php echo $aset->nama_aset ?></td>
            <td><?php echo $aset->sn_aset ?></td>
            <td><?php echo $aset->kondisi ?></td>
            <td>
            </td>
        </tr>
        <?php } ?>
    </table>
</body>
<br>

<div class="row mt-12">
                         <div class="col-xs-10">

                    </div>
                    <div class="col-xs-10">
                        <table align="right">

                            <tr>
                                <td class="text-center"> <b> Dumai, <?php echo date('d M Y'); ?></b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> Mengetahui</b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> PIC </b></td>
                            </tr>

                            <tr>
                                <td>
                                    <br>
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <u><b>MUHAMAD TAMRIN</b></u>
                                </td>
                            </tr>
                        </table>
                    </div>
</div>
</html>