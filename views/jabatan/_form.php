<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\jabatan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label">ID Jabatan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'id_jabatan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Nama Jabatan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'n_jabatan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
    </div>

	<div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
