<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
             <p>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-plus"></i> Add Data User'), ['create'], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
              <!--   <?= Html::a(Yii::t('app', '<i class="ico fa fa-download"></i> Export Data User'), ['download'], ['class' => 'btn btn-icon btn-icon-left btn-default btn-xs waves-effect waves-light']) ?> -->
            </p>
                <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout'=>"{items}\n{summary}\n{pager}",
                    'options' =>['class' => 'text-center'],
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'contentOptions' =>['class' => 'table-responsive'],
                        ],

                   
                        'username',
         

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'header'=>'Action', 
                             'contentOptions' =>['style' => 'width:13%;'],
                            'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-edit"></span>', 
                                            $url,['class'=>'btn btn-success btn-circle btn-xs waves-effect waves-light']);
                                    },
                                    'view' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-eye-open"></span>', 
                                            $url,['class'=>'btn btn-info btn-circle btn-xs waves-effect waves-light']);
                                    },
                                    'delete' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-remove"></span>', 
                                            $url,['class'=>'btn btn-danger btn-circle btn-xs waves-effect waves-light']);
                                    },
                                    'link' => function ($url,$model,$key) {
                                            return Html::a('Action', $url);
                                    },
                                ],
                            ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
