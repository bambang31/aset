<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>
    <div class="form-group">
        <label class="col-sm-2 control-label">id</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'id')->textInput(['class'=>'form-control'])->label(false) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'username')->textInput(['class'=>'form-control'])->label(false) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'auth_key')->textInput(['class'=>'form-control'])->label(false) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Ulangi Password</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'password_hash')->textInput(['class'=>'form-control'])->label(false) ?>
        </div>
    </div>
   
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
   

    <?php ActiveForm::end(); ?>

</div>
