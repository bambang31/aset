<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ruangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_ruangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_ruangan')->textInput() ?>

    <?= $form->field($model, 'nama_karyawan')->textInput() ?>

    <?= $form->field($model, 'lokasi')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
