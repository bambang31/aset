<?php

namespace app\controllers;
/* @var $this yii\web\View */
use Yii;
use app\models\pemutihan;
use app\models\aset;
use app\models\karyawan;
use app\models\ruangan;
use app\models\mutasi;
use app\models\jabatan;

$this->title = 'Sistem Inventory Aset';
?>
<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">   
            <h3><?php $aset = Aset::find()->count();
                echo "$aset";
             ?><sup style="font-size: 20px"></sup></h3>
              <p>Data Aset</p>
            </div>
            <div class="icon">
              <i class="ion ion-briefcase  "></i>
            </div>
            <a href="./index.php?r=aset" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php $karyawan = Karyawan::find()->count();
                echo "$karyawan";
             ?><sup style="font-size: 20px"></sup></h3>

              <p>Data Karyawan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="./index.php?r=karyawan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php $ruangan = Ruangan::find()->count();
                echo "$ruangan";
             ?></h3>

              <p>Data Ruangan</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="./index.php?r=ruangan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><?php $jabatan = Jabatan::find()->count();
                echo "$jabatan";
             ?></h3>

              <p>Data Jabatan</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="./index.php?r=ruangan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php $mutasi = Mutasi::find()->count();
                echo "$mutasi";
             ?></h3>

              <p>Data Mutasi</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="./index.php?r=mutasi" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue ">
            <div class="inner">
              <h3><?php $pemutihan = Pemutihan::find()->count();
                echo "$pemutihan";
             ?></h3>

              <p>Data Pemutihan</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="./index.php?r=pemutihan" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

</div>
