<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\pemutihan */

$this->title = 'Update Pemutihan: ' . $model->kode_pemutihan;
$this->params['breadcrumbs'][] = ['label' => 'Pemutihan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_pemutihan, 'url' => ['view', 'id' => $model->kode_pemutihan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pemutihan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
