 <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ruangan;
use app\models\aset;
use app\models\karyawan;
use app\models\pemutihan;
use kartik\select2\select2;
use yii\helpers\jsonParse;
use yii\helpers\json;
use yii\helpers\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\pemutihan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Pemutihan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kode_pemutihan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kode Aset</label>
        <div class="col-sm-10">
             <?= $form->field($model, 'kode_aset')-> widget (select2::classname(),[
                'data' => ArrayHelper::map(aset::find()-> all(),'kode_aset','kode_aset'),
                'language' => 'en',
                'options' => ['placeholder' => 'Pilih Kode Aset','id'=>'asetID'],
                'pluginOptions' =>[
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>
        <label class="col-sm-2 control-label">Nama Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'nama_aset')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kondisi Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kondisi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Lokasi Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Keterangan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">ID Karyawan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'id_karyawan')-> widget (select2::classname(),[
                'data' => ArrayHelper::map(karyawan::find()-> all(),'id_karyawan','id_karyawan'),
                'language' => 'en',
                'options' => ['placeholder' => 'Pilih ID Karyawan','id'=>'jabatID'],
                'pluginOptions' =>[
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>
        <label class="col-sm-2 control-label">Nama Karyawan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'nama_karyawan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>


<?php 
$script = <<< JS
$('#asetID').change(function(){
    var asetID = $(this).val();

    $.get('index.php?r=pemutihan/get-aset',{ asetID : asetID },function(data){
        var data = $.parseJSON(data);  
        console.log(data)
        $('#pemutihan-nama_aset').val(data.nama_aset);
        $('#pemutihan-kondisi').val(data.kondisi);
        $('#pemutihan-lokasi').val(data.lokasi);
        $('#pemutihan-keterangan').val(data.keterangan);
        });
});
JS;
$this->registerJS($script);
?>  

<?php 
$script = <<< JS
$('#jabatID').change(function(){
    var jabatID = $(this).val();
    $.get('index.php?r=pemutihan/get-jabatan',{ jabatID : jabatID },function(data){
        var data = $.parseJSON(data);  
        console.log(data)
        $('#pemutihan-nama_karyawan').val(data.nama_karyawan);
        $('#pemutihan-jabatan').val(data.jabatan);
        });
});
JS;
$this->registerJS($script);
?>  