<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\pemutihan */

$this->title = 'Data Pemutihan '.$model->kode_pemutihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Pemutihan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-aset-view">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
                <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
            'kode_pemutihan',
            'kode_aset',
            'nama_aset',
            'kondisi',
            'lokasi',
            'keterangan',
            'id_karyawan',
            'nama_karyawan',
            'jabatan',
        ],
                ]) ?>
                <p>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Print'), ['gen-pdf', 'id' => $model->kode_pemutihan], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Update'), ['update', 'id' => $model->kode_pemutihan], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>

               <?= Html::a(Yii::t('app', '<i class="ico fa fa-remove"></i> Delete'), ['delete', 'id' => $model->kode_pemutihan], [
                    'class' => 'btn btn-icon btn-icon-left btn-danger btn-xs waves-effect waves-light',
                    'data' => [
                        'confirm' => 'Anda yakin akan menghapus data pemutihan? Seluruh data akan terhapus!',
                        'method' => 'POST',
                    ],
                ]) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
            </p>
            </div>
        </div>
    </div>

</div>
