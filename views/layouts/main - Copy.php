<?php
 
/* @var $this \yii\web\View */
/* @var $content string */
 
use app\assets\DashboardAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
 
DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
</body>
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Alexander Pierce</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li class="active treeview">
            <a href="/aset/web/index.php?">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>Data Master</span>
              <span class="label label-primary pull-right">3</span>
            </a>
            <ul class="treeview-menu">
              <li><a href="./index.php?r=aset"><i class="fa fa-circle-o"></i>Data Aset</a></li>
              <li><a href="./index.php?r=karyawan"><i class="fa fa-circle-o"></i> Data Karyawan</a></li>
              <li><a href="./index.php?r=ruangan"><i class="fa fa-circle-o"></i> Data Ruangan</a></li>
            </ul>
          </li>
          <li>
            <a href="pages/widgets.html">
              <i class="fa fa-th"></i> <span>Inventaris</span> <small class="label pull-right bg-green">2</small>
             <ul class="treeview-menu">
              <li><a href="./index.php?r=mutasi"><i class="fa fa-circle-o"></i>Data Mutasi</a></li>
              <li><a href="./index.php?r=pemutihan"><i class="fa fa-circle-o"></i> Data Pemutihan</a></li>
            </ul>
            </a>
          </li>
        </ul>
      </section>
    </div>
      <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                Dashboard
                <small>Control panel</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
              </ol>
            </section>
 
            <!-- Main content -->
        <section class="content">
            <?= $content ?>
        </section>
        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Code Focus Develpoer <?= date('Y') ?></p>
 
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>
    </div>
</div>
 
 
 <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>