<?php

use app\vendor\adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
<?= Html::img('@web/img/1.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?=
        Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 
                            'url' => ['/'], 'active' => $this->context->route == 'site/index'
                        ],
                        [
                            'label' => 'Data Master',
                            'icon' => 'fa fa-database',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Data Aset',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=aset/',
				    'active' => $this->context->route == 'aset/index'
                                ],
                                [
                                    'label' => 'Data Karyawan',
                                    'icon' => 'fa fa-users',
                                    'url' => '?r=karyawan/',
                    'active' => $this->context->route == 'karyawan/index'
                                ],
                                [
                                    'label' => 'Data Ruangan',
                                    'icon' => 'fa fa-building',
                                    'url' => '?r=ruangan/',

                    'active' => $this->context->route == 'ruangan/index'
                                ],
                                [
                                    'label' => 'Data Jabatan',
                                    'icon' => 'fa fa-graduation-cap',
                                    'url' => '?r=jabatan/',

				    'active' => $this->context->route == 'jabatan/index'
                                ]
                            ]
                        ],
                        [
                            'label' => 'Inventaris',
                            'icon' => 'fa fa-briefcase',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Data Mutasi',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=mutasi/',
                    'active' => $this->context->route == 'mutasi/index'
                                ],

                                [
                                    'label' => 'Data Pemutihan',
                                    'icon' => 'fa fa-trash',
                                    'url' => '?r=pemutihan/',
                    'active' => $this->context->route == 'pemutihan/index'
                                ]
                            ]
                        ],
                        [
                            'label' => 'Users',
                            'icon' => 'fa fa-users',
                            'url' => ['/user'],
                            'active' => $this->context->route == 'user/index',
                        ],
                    ],
                ]
        )
        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>
