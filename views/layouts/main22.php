<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$urlMenu = Yii::$app->urlManager->parseRequest(Yii::$app->request);
$urlA = explode("/", $urlMenu[0]);
setlocale(LC_MONETARY, 'id_ID'); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php if (!Yii::$app->user->isGuest): ?>
   <div class="main-menu">
    <header class="header">
        <a href="index.html" class="logo">NBC Admin</a>
        <button type="button" class="button-close fa fa-times js__menu_close"></button>
        <div class="user">
            <a href="#" class="avatar"><img src="../assets/images/admin.png" alt=""><span class="status online"></span></a>
            <h5 class="name"><a href="#"><?=  Yii::$app->user->identity->username ?></a></h5>
            <h5 class="position">Administrator</h5>
            <!-- /.name -->
            <div class="control-wrap js__drop_down">
                <i class="fa fa-caret-down js__drop_down_button"></i>
                <div class="control-list">
                    <div class="control-item"><a href="#"><i class="fa fa-gear"></i> Pengaturan</a></div>
                    <div class="control-item"><a href="<?= URL::to(['web/logout']) ?>" data-method="post"><i class="fa fa-sign-out"></i> Log out</a></div>
                </div>
                <!-- /.control-list -->
            </div>
            <!-- /.control-wrap -->
        </div>
        <!-- /.user -->
    </header>
    <!-- /.header -->
    <div class="content mCustomScrollbar _mCS_1"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">

        <div class="navigation">
            <h5 class="title">Navigation</h5>
            <!-- /.title -->
            <ul class="menu js__accordion">
                <li class="<?php echo ($urlA[0] == 'web' && $urlA[1] =='index' ) ? 'current' : '' ; ?>">
                    <a class="waves-effect" href="<?= URL::to(['web/index']) ?>"><i class="menu-icon fa fa-home"></i><span>Dashboard</span></a>
                </li>
                <li class="<?php echo ($urlA[0] == 'proses' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                    <a class="waves-effect" href="<?= URL::to(['proses/index']) ?>"><i class="menu-icon fa fa-refresh"></i><span>Proses NBC</span></a>
                </li>
                <li class="<?php echo ($urlA[0] == 'testing' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                    <a class="waves-effect" href="<?= URL::to(['testing/index']) ?>"><i class="menu-icon fa fa-database"></i><span>Data Testing</span></a>
                </li>
                 <li class="<?php echo ($urlA[0] == 'penjualan' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                    <a class="waves-effect" href="<?= URL::to(['penjualan/index']) ?>"><i class="menu-icon fa fa-database"></i><span>Data Training</span></a>
                </li>

                <li class="<?php echo ($urlA[0] == 'merk' || $urlA[0] == 'type' || $urlA[0] == 'warna') ? 'current active"' : '' ; ?>">
                    <a class="waves-effect parent-item js__control" href="#"><i class="menu-icon fa fa-random"></i><span>Master Data</span><span class="menu-arrow fa fa-angle-down"></span></a>
                    <ul class="sub-menu js__content">
                         <li class="<?php echo ($urlA[0] == 'merk' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                            <a class="waves-effect" href="<?= URL::to(['merk/index']) ?>"><span>Data Merk</span></a>
                        </li>
                         <li class="<?php echo ($urlA[0] == 'type' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                            <a class="waves-effect" href="<?= URL::to(['type/index']) ?>"><span>Data Type</span></a>
                        </li>
                         <li class="<?php echo ($urlA[0] == 'warna' && ($urlA[1] =='index' || $urlA[1] == 'create' || $urlA[1] == 'update' || $urlA[1] == 'view')) ? 'current' : '' ; ?>">
                            <a class="waves-effect" href="<?= URL::to(['warna/index']) ?>"></i><span>Data Warna</span></a>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo ($urlA[0] == 'user' && $urlA[1] =='index' ) ? 'current' : '' ; ?>">
                    <a class="waves-effect" href="<?= URL::to(['user/index']) ?>"><i class="menu-icon fa fa-users"></i><span>Data User</span></a>
                </li>
                <li class="">
                    <a class="waves-effect" href="<?= URL::to(['web/logout']) ?>" data-method="post"><i class="menu-icon fa fa-sign-out"></i><span>Logout</span></a>
                </li>
            </ul>            
          
        </div>
        <!-- /.navigation -->
    </div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: block; height: 55px; max-height: 192px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
    <!-- /.content -->
</div>

        <div class="fixed-navbar">
            <div class="pull-left">
                <button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
            </div>
        </div>

    <div id="wrapper">
        <div class="main-content">
            <div class="row small-spacing">
                <div class="col-lg-12">
                    <?= Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]) ?>
                </div>
                <div class="col-lg-12">
                    <?= Alert::widget() ?>
                </div>
<?php endif ?>                
                <?= $content ?>
<?php if (!Yii::$app->user->isGuest): ?>                
            </div>
            <footer class="footer">
                <ul class="list-inline">
                    <li><a href="http://www.java-sc.com" target="_blank">Development</a></li>
                    <li><?php echo date('Y'); ?> © NBC Admin.</li>
                </ul>
            </footer>
        </div>
    </div>
<?php endif ?>  
<?php $this->endBody() ?>
<script type="text/javascript">
        //rupiah
        /* Dengan Rupiah */
            var dengan_rupiah = document.getElementById('harga');
            if (dengan_rupiah != null) {
                dengan_rupiah.addEventListener('keyup', function(e){
                    dengan_rupiah.value = formatRupiah(this.value);
                });
            
                dengan_rupiah.addEventListener('keydown', function(event) {
                    limitCharacter(event);
                });
            }
            
            /* Fungsi */
            function formatRupiah(bilangan, prefix)
            {
                var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                    split   = number_string.split(','),
                    sisa    = split[0].length % 3,
                    rupiah  = split[0].substr(0, sisa),
                    ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
                    
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
            
            function limitCharacter(event)
            {
                key = event.which || event.keyCode;
                if ( key != 188 // Comma
                     && key != 8 // Backspace
                     && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
                     && (key < 48 || key > 57) // Non digit
                     // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
                    ) 
                {
                    event.preventDefault();
                    return false;
                }
            }

        //rupiah !
        String.prototype.capitalize = function(){
            return this.toLowerCase().replace( /\b\w/g, function (m) {
                return m.toUpperCase();
            });
        };

        function capitalize(textboxid, str) {
            document.getElementById(textboxid).value = str.capitalize();
        }

        $('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });
        
        (function($) {
            "use strict";
            $('.dropify').dropify();
        })(jQuery);

        $('.select2_1').length;
        $(".select2_1").select2();
        yii.confirm = function (message, okCallback, cancelCallback) {
            swal({
                title: message,
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: true,
                allowOutsideClick: true
            }, okCallback);
        };
</script>
</body>
</html>
<?php $this->endPage() ?>
