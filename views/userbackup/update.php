<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\user */

$this->title = Yii::t('app', 'Ubah Data User {nameAttribute}', [
    'nameAttribute' => $model->username,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->username]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Ubah Data User');
?>

<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

