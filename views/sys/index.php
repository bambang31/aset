<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="single-wrapper">
    <!-- <form action="#" class="frm-single"> -->
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'frm-single']]); ?>
        <div class="inside">
            <div class="title"><strong>FPNN</strong> Admin</div>
            <!-- /.title -->
            <div class="frm-title">Login Page</div>
            <!-- /.frm-title -->
            <div class="frm-input">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Username','class'=>'frm-inp'])->label(false) ?>
            </div>
            <!-- /.frm-input -->
            <div class="frm-input">
                <?= $form->field($model, 'password')->passwordInput(['class'=>'frm-inp','placeholder'=>'Password'])->label(false) ?>
            </div>
            <!-- /.frm-input -->
            <div class="clearfix margin-bottom-20">
                <div class="pull-left">
                    <div class="checkbox primary"><input type="checkbox" id="rememberme"><label for="rememberme">Remember me</label></div>
                    <!-- /.checkbox -->
                </div>
                <!-- /.pull-left -->
                <div class="pull-right"><a href="page-recoverpw.html" class="a-link"><i class="fa fa-unlock-alt"></i>Forgot password?</a></div>
                <!-- /.pull-right -->
            </div>
            <!-- /.clearfix -->
             <?= Html::submitButton('Login <i class="fa fa-arrow-circle-right"></i>', ['class' => 'frm-submit', 'name' => 'login-button']) ?>
            <!-- /.row -->
            <div class="frm-footer">Bambang © <?= date('Y') ?>.</div>
            <!-- /.footer -->
        </div>
        <!-- .inside -->
    <!-- </form> -->
    <?php ActiveForm::end(); ?>
    <!-- /.frm-single -->
</div><!--/#single-wrapper -->
