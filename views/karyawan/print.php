<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\aset */

$this->title = 'Data Karyawan '.$model->id_karyawan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Karyawan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-aset-view">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                    'id_karyawan',
                    'nama_karyawan',
                    'jabatan',
                    'alamat',
                 ],
                ]) ?>
            </div>
        </div>
    </div>

</div>
<html>
<div class="row mt-12">
                         <div class="col-xs-10">

                    </div>
                    <div class="col-xs-10">
                        <table align="right">

                            <tr>
                                <td class="text-center"> <b> Dumai, <?php echo date('d M Y'); ?></b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> Mengetahui</b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> PIC </b></td>
                            </tr>

                            <tr>
                                <td>
                                    <br>
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <u><b>MUHAMAD TAMRIN</b></u>
                                </td>
                            </tr>
                        </table>
                    </div>
</div>
</html>