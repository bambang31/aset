<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Jabatan;

/* @var $this yii\web\View */
/* @var $model app\models\karyawan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label">Karyawan ID</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'id_karyawan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label">Nama Karyawan</label>
        <div class="col-sm-10">
        <?= $form->field($model, 'nama_karyawan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <div class="form-group">
        <label class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-10">
        <?= $form->field($model, 'jabatan')->dropDownList(
        ArrayHelper::map(Jabatan::find()->all(),'n_jabatan','n_jabatan'),
        ['prompt'=>'Select Jabatan']
    ) ->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Alamat</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'alamat')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
    </div>

	<div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
