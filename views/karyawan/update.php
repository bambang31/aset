<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\karyawan */

$this->title = Yii::t('app', 'Ubah Data karyawan {nameAttribute}', [
    'nameAttribute' => $model->id_karyawan,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data karyawan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_karyawan, 'url' => ['view', 'id' => $model->id_karyawan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Ubah Data karyawan');
?>

<div class="karyawan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>