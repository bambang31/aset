<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\karyawan */

$this->title = Yii::t('app', 'Tambah Data karyawan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data karyawan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-merk-create">
    <div class="col-lg-12">
		<div class="box-content card danger">
			<h4 class="box-title"><?= Html::encode($this->title) ?></h4>
			<div class="card-content">
				<?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>
			</div>
		</div>
	</div>
</div>
