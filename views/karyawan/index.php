<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KaryawanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Karyawan - Aset Admin');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-karyawan-index">
   
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
           <!--  <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content"> -->
             <p>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-plus"></i> Add Data Karyawan'), ['create'], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
               <!-- <?= Html::a(Yii::t('app', '<i class="ico fa fa-plus"></i> Export/ Print Data Karyawan'), ['gen-pdf'], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?> -->
            </p>
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="table-responsive">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <?=
                     Alert::widget([
                        'useSessionFlash' => false,
                        'options' => [
                            'title' => "Data Berhasil Dihapus",
                            'type' => Alert::TYPE_SUCCESS
                        ]
                    ]);
                    ?>
                <?php endif ?>
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <?=
                     Alert::widget([
                        'useSessionFlash' => false,
                        'options' => [
                            'title' => "Data Gagal Dihapus",
                            'type' => Alert::TYPE_ERROR
                        ]
                    ]);
                    ?>
                <?php endif ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'layout'=>"{items}\n{summary}\n{pager}",
                    'options' =>['class' => 'text-left'],
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'contentOptions' =>['class' => 'table-responsive'],
                        ],

                        'id_karyawan',
                        'nama_karyawan',
                        'jabatan',
                        'alamat',

                       [
                         'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update}',
                            'header'=>'Action', 
                             'contentOptions' =>['style' => 'width:13%;'],
                            'buttons' => [
                                    'update' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-edit"></span>', 
                                            $url,['class'=>'btn btn-success btn-circle btn-xs waves-effect waves-light']);
                                    },
                                    'view' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-eye-open"></span>', 
                                            $url,['class'=>'btn btn-info btn-circle btn-xs waves-effect waves-light']);
                                    },
                                    'delete' => function ($url,$model) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-remove"></span>', 
                                            $url,[
                                            'class'=>'btn btn-danger btn-circle btn-xs waves-effect waves-light',
                                            'data' => [
                                                    'confirm' => 'Anda yakin akan menghapus data testing? Seluruh data akan terhapus!',
                                                    'method' => 'POST',
                                                ],
                                            ]);
                                    },
                                    'link' => function ($url,$model,$key) {
                                            return Html::a('Action', $url);
                                    },
                                ],
                        ],
                    ],
                ]); ?>
           
                </div>
            </div>
        </div>
    </div>
</div>