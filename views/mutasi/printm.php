<?php

// print_r($model);
// exit();

use yii\helpers\Html;
use yii\grid\GridView;
use yii\mPDF;
use app\models\mutasi;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\mutasi */

$this->title = Yii::t('app', 'Data Mutasi - Aset Admin');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-aset-view">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
            'kode_mutasi',
            'kode_ruangan',
            'nama_ruangan',
            // 'id_karyawan',
            // 'nama_karyawan',
            'nama_aset',
            'kondisi',
            'tgl_diterima',
        ],
                ]) ?>
            </div>
        </div>
    </div>

</div>
<!DOCTYPE html>
<html>
<div class="row mt-12">
                         <div class="col-xs-10">

                    </div>
                    <div class="col-xs-10">
                        <table align="right">

                            <tr>
                                <td class="text-center"> <b> Dumai, <?php echo date('d M Y'); ?></b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> Mengetahui</b></td>
                            </tr>
                             <tr>
                                <td class="text-center"> <b> PIC </b></td>
                            </tr>

                            <tr>
                                <td>
                                    <br>
                                    <br>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <u><b>MUHAMAD TAMRIN</b></u>
                                </td>
                            </tr>
                        </table>
                    </div>
</div>
</html>