<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\mutasi */

$this->title = 'Data Mutasi '.$model->kode_mutasi;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Mutasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-aset-view">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="box-content card danger">
            <h4 class="box-title"><?= Html::encode($this->title) ?></h4>
            <div class="card-content">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
            'kode_mutasi',
            'kode_ruangan',
            'nama_ruangan',
            // 'id_karyawan',
            // 'nama_karyawan',
            'nama_aset',
            'kondisi',
            'tgl_diterima',
        ],
                ]) ?>
                <p>
               <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Print'), ['gen-pdf', 'id' => $model->kode_mutasi], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-edit"></i> Update'), ['update', 'id' => $model->kode_mutasi], ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
               <?= Html::a(Yii::t('app', '<i class="ico fa fa-remove"></i> Delete'), ['delete', 'id' => $model->kode_mutasi], [
                    'class' => 'btn btn-icon btn-icon-left btn-danger btn-xs waves-effect waves-light',
                    'data' => [
                        'confirm' => 'Anda yakin akan menghapus data mutasi? Seluruh data akan terhapus!',
                        'method' => 'POST',
                    ],
                ]) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
            </p>
            </div>
        </div>
    </div>

</div>
