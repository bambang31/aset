<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MutasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-mutasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

     <div class="row">
        <div class="form-group">
             <div class="col-sm-12">
                <?= $form->field($model, 'kode_mutasi')->textInput(['class'=>'form-control','style'=>'text-transform:capitalize;','placeholder'=>'Masukan Kode Mutasi'])->label(false) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-search"></i> Search'), ['class' => 'btn btn-icon btn-icon-left btn-success btn-xs waves-effect waves-light']) ?>
                <?= Html::a(Yii::t('app', '<i class="ico fa fa-refresh"></i> Reset'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
