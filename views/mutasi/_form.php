<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ruangan;
use app\models\aset;
use yii\helpers\ArrayHelper;
use kartik\select2\select2;
use kartik\date\DatePicker;
use yii\helpers\json;
use yii\helpers\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\models\mutasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">

    <?php $form = ActiveForm::begin(['class'=>'form-horizontal']); ?>

    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Mutasi</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kode_mutasi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kode Ruangan</label>
        <div class="col-sm-10">
             <?= $form->field($model, 'kode_ruangan')-> widget (select2::classname(),[
                'data' => ArrayHelper::map(ruangan::find()-> all(),'kode_ruangan','kode_ruangan'),
                'language' => 'en',
                'options' => ['placeholder' => 'Pilih Kode Ruangan','id'=>'ruangID'],
                'pluginOptions' =>[
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>
        <label class="col-sm-2 control-label">Nama Ruangan</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'nama_ruangan')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kode Aset</label>
        <div class="col-sm-10">
             <?= $form->field($model, 'kode_aset')-> widget (select2::classname(),[
                'data' => ArrayHelper::map(aset::find()-> all(),'kode_aset','kode_aset'),
                'language' => 'en',
                'options' => ['placeholder' => 'Pilih Kode Aset','id'=>'setID'],
                'pluginOptions' =>[
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>
        <label class="col-sm-2 control-label">Nama Aset</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'nama_aset')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
        <label class="col-sm-2 control-label">Kondisi</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'kondisi')->textInput(['maxlength' => true,'required'=>'required','onkeyup'=>'javascript:capitalize(this.id, this.value);'])->label(false) ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Diterima</label>
        <div class="col-sm-10">
            <?= $form->field($model, 'tgl_diterima')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Masukkan tanggal ...'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy/mm/dd'
    ] 
])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('app', '<i class="ico fa fa-plus"></i> Simpan'), ['class' => 'btn btn-icon btn-icon-left  btn-success btn-xs waves-effect waves-light']) ?>
            <?= Html::a(Yii::t('app', '<i class="ico fa fa-arrow-left"></i> Kembali'), ['index'], ['class' => 'btn btn-icon btn-icon-left btn-info btn-xs waves-effect waves-light']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS
$('#ruangID').change(function(){
    var ruangID = $(this).val();

    $.get('index.php?r=mutasi/get-ruang',{ ruangID : ruangID },function(data){
        var data = $.parseJSON(data);  
        console.log(data)
        $('#mutasi-nama_ruangan').val(data.nama_ruangan);
        });
});
JS;
$this->registerJS($script);
?>  

<?php 
$script = <<< JS
$('#namaID').change(function(){
    var namaID = $(this).val();

    $.get('index.php?r=mutasi/get-nama',{ namaID : namaID },function(data){
        var data = $.parseJSON(data);  
        console.log(data)
        $('#mutasi-nama_karyawan').val(data.nama_karyawan);
        });
});
JS;
$this->registerJS($script);
?>  

<?php 
$script = <<< JS
$('#setID').change(function(){
    var setID = $(this).val();

    $.get('index.php?r=mutasi/get-aset',{ setID : setID },function(data){
        var data = $.parseJSON(data);  
        console.log(data)
        $('#mutasi-nama_aset').val(data.nama_aset);
        $('#mutasi-kondisi').val(data.kondisi);
        });
});
JS;
$this->registerJS($script);
?>  