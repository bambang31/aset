<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\mutasi */

$this->title = Yii::t('app', 'Ubah Data Mutasi {nameAttribute}', [
    'nameAttribute' => $model->kode_mutasi,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Mutasi'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode_mutasi, 'url' => ['view', 'id' => $model->kode_mutasi]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Ubah Data Mutasi');
?>

<div class="aset-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
