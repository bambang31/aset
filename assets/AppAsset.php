<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/styles/style.min.css',
        'assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css',
        'assets/plugin/waves/waves.min.css',
        'assets/plugin/sweet-alert/sweetalert.css',
        'assets/styles/color/red.min.css',
        'assets/plugin/datepicker/css/bootstrap-datepicker.min.css',
        'assets/plugin/waves/waves.min.css',
        'assets/plugin/select2/css/select2.min.css',
        'assets/plugin/dropify/css/dropify.min.css',
    ];
    public $js = [
    	'assets/scripts/jquery.min.js',
    	'assets/scripts/modernizr.min.js',
    	'assets/plugin/bootstrap/js/bootstrap.min.js',
    	'assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js',
    	'assets/plugin/nprogress/nprogress.js',
    	'assets/plugin/sweet-alert/sweetalert.min.js',
    	'assets/plugin/waves/waves.min.js',
        'assets/plugin/datepicker/js/bootstrap-datepicker.min.js',
        'assets/plugin/select2/js/select2.min.js',
        'assets/plugin/dropify/js/dropify.min.js',
    	'assets/scripts/main.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
