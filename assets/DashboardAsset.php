<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'font-awesome-4.7.0/css/font-awesome.min.css',
        'css/skins/_all-skins.min.css',
        'css/adminLTE.min.css',
        'css/site.css',
        'css/Ionicons/css/ionicons.min.css',
        'plugins/icheck/flat/blue.css',
        'js/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'js/bootstrap-daterangepicker/daterangepicker.css',
        'js/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'js/jvectormap/jquery-jvectormap.css',
        'js/morris.js/morris.css',

    ];
    public $js = [
        'js/bootstrap/bootstrap.min.js',
        'js/jquery-ui/jquery-ui.min.js',
        'js/jquery-sparkline/dist/jquery.sparkline.min.js',
        'js/jquery-slimscroll/jquery.slimscroll.min.js',
        'js/fastclick/lib/fastclick.js',
        'js/raphael/raphael.min.js',
        'js/morris/morris.min.js',
        'js/jvectormap/jquery-jvectormap-1.2.2.js',
        'js/app.min.js',
        'js/dashboard.js',
        'js/main.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
