<?php

namespace app\controllers;

use Yii;
use kartik\mpdf\Pdf;
use app\models\pemutihan;
use app\models\aset;
use app\models\karyawan;
use app\models\ruangan;
use app\models\PemutihanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\json;
use yii\helpers\jsonParse;

/**
 * PemutihanController implements the CRUD actions for pemutihan model.
 */
class PemutihanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all pemutihan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PemutihanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single pemutihan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new pemutihan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate()
{
    $model = new pemutihan();

    if ($model->load(Yii::$app->request->post()) ) {

        if($model->save()) {

            $model_aset = Aset::find()->where(['kode_aset'=>$model->kode_aset])->one();
            if($model_aset->delete()) {
                return $this->redirect(['view', 'id' => $model->kode_pemutihan]);
            } 
        } 
    }

    return $this->render('create', [
        'model' => $model,
    ]);  
    
}

    /**
     * Updates an existing pemutihan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kode_pemutihan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing pemutihan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


public function actionGenPdf($id){
        $pemutihan = $this->findModel($id);
         $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('printp',[
                             'model'   => $pemutihan]),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Sistem Inventory - Aset Admin',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Laporan Data Pemutihan||Dibuat tanggal: ' . date("r")],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetAuthor' => 'Bambang Setiawan',
                    'SetCreator' => 'Bambang Setiawan',
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            return $pdf->render();
        }

public function actionGetAset($asetID)
        {
            //mencari kode aset dari table pemutihan
            $aset = aset::findOne($asetID);
            return Json::encode($aset);
        }


public function actionGetJabatan($jabatID)
        {
            //mencari jabatan dari table karyawan
            $karyawan = karyawan::findOne($jabatID);
            return Json::encode($karyawan);
        }
    /**
     * Finds the pemutihan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return pemutihan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = pemutihan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
