<?php

namespace app\controllers;

use Yii;
use kartik\mpdf\Pdf;
use app\models\mutasi;
use app\models\MutasiSearch;
use app\models\ruangan;
use app\models\aset;
use app\models\karyawan;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\json;

/**
 * MutasiController implements the CRUD actions for mutasi model.
 */
class MutasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all mutasi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MutasiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single mutasi model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new mutasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new mutasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kode_mutasi]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionGenPdf($id){
        $mutasi = $this->findModel($id);
         $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('printm',[
                             'model'   => $mutasi]),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Sistem Inventory - Aset Admin',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Laporan Data Mutasi||Dibuat tanggal: ' . date("r")],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetAuthor' => 'Bambang Setiawan',
                    'SetCreator' => 'Bambang Setiawan',
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            return $pdf->render();
        }

    /**
     * Updates an existing mutasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kode_mutasi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing mutasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetRuang($ruangID)
        {
            //mencari kode ruangan dari table ruangan
            $ruangan = ruangan::findOne($ruangID);
            return Json::encode($ruangan);
        }

    public function actionGetNama($namaID)
        {
            //mencari nama karyawan dari table karyawan
            $karyawan = karyawan::findOne($namaID);
            return Json::encode($karyawan);
        }

        public function actionGetAset($setID)
        {
            //mencari nama karyawan dari table karyawan
            $aset = aset::findOne($setID);
            return Json::encode($aset);
        }

    /**
     * Finds the mutasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return mutasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = mutasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
