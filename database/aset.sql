-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Sep 2020 pada 16.27
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aset_bambang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset`
--

CREATE TABLE `aset` (
  `kode_aset` varchar(11) NOT NULL,
  `nama_aset` varchar(50) DEFAULT NULL,
  `sn_aset` varchar(25) DEFAULT NULL,
  `lokasi` varchar(20) NOT NULL,
  `keterangan` varchar(20) NOT NULL,
  `kondisi` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset`
--

INSERT INTO `aset` (`kode_aset`, `nama_aset`, `sn_aset`, `lokasi`, `keterangan`, `kondisi`) VALUES
('318089', 'Panci', '328721809', 'Sanhok', 'Banyak begal', 'HB'),
('71260175976', 'AMPLIFIER TRUNK XHIR 1115/G2', '1115/G2', 'Pekanbaru', 'Baru saja dibeli', 'HD'),
('71260176259', 'AMPLIFIER TBA XHITA 068242', '068242', 'Sanhok', 'Banyak begal', 'BR'),
('71310196285', 'BKS OUT STATIO XFAR    SCADA  \r\n', 'SCADA', '', '', 'HD'),
('71314139996', 'BASE STATION XGE', 'XGE', '', '', 'RS'),
('71314175699', 'VOTYNG SELECTOR XGE 8213910', '8213910', '', '', 'HD'),
('71316139470', 'RADIO MOBILE XGE MC66KHS', '081077', '', '', 'HD'),
('71316139472', 'RADIO MOBILE XGE MC66KHS', '081079', '', '', 'HD'),
('71316140252', 'RADIO MOBILE XGE MASTER', '451288', '', '', 'HD'),
('71316140467', 'MITREK XMOT 34JJA19', 'GE2452', '', '', 'HD'),
('71316140470', 'MITREK XMOT 34JJA19', 'GE2455', '', '', 'HD'),
('71316174816', 'RADIO MOBILE XMOT PMC4032', 'PMC4032', '', '', 'HD'),
('71316174862', 'RADIO MOBILE XMOT HJA0883', 'HJA0883', '', '', 'HD'),
('71316174863', 'RADIO MOBILE XMOT HJA0884', 'HJA0884', '', '', 'HD'),
('71460139855', 'MONITOROFM XCH 305', '1828', '', '', 'HD'),
('71610368514', 'UPS: APC SMART XL 3000VA RACK', '3000VA RACK', '', '', 'BR'),
('71610368530', 'UPS: APC SMART XL 3000VA RACK\r\n', '3000VA RACK', '', '', 'BR'),
('72191190536', 'COMPUTER ELCOM ST3000', 'ST3000', '', '', 'HD'),
('7777', 'Laptop', '38uninn8', '', '', 'Baru'),
('99135240427', 'CATV VIDEO SYST 522', '522', '', '', 'HD'),
('WUWEURE', 'IDEPRGI', 'JDW;PRGUT', '', '', 'DWJPFI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` varchar(20) NOT NULL,
  `n_jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `n_jabatan`) VALUES
('001', 'Manager Operasional'),
('002', 'Teknisi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `id_karyawan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nama_karyawan`, `jabatan`, `alamat`) VALUES
('112', 'Bambang Setiawan', 'Manager', 'Pekanbaru'),
('113', 'sukoco', 'Teknisi', 'Dumai'),
('211', 'Joko susilo', 'Manager Operasional', 'Pekanbaru'),
('335', 'SBY', 'Teknisi', 'd');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mutasi`
--

CREATE TABLE `mutasi` (
  `kode_mutasi` varchar(11) NOT NULL,
  `kode_ruangan` varchar(50) NOT NULL,
  `nama_ruangan` varchar(50) NOT NULL,
  `id_karyawan` varchar(11) NOT NULL,
  `nama_karyawan` varchar(20) NOT NULL,
  `kode_aset` varchar(20) NOT NULL,
  `nama_aset` varchar(50) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `tgl_diterima` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mutasi`
--

INSERT INTO `mutasi` (`kode_mutasi`, `kode_ruangan`, `nama_ruangan`, `id_karyawan`, `nama_karyawan`, `kode_aset`, `nama_aset`, `kondisi`, `tgl_diterima`) VALUES
('32', '002', 'Anggrek Office', '113', 'sukoco', '', 'Kulkas', 'bekas', '2020-08-07'),
('4238428', '002', 'Mesin', '112', 'Joko', '', 'Piring', 'bekas', '2020-08-04'),
('834u398u9', '001', 'Piring', '113', 'Bambang', '', 'Kulkas', 'Bekas', '2020-08-04'),
('8p9up9', '002', 'Anggrek Office', '113', 'sukoco', '', 'Kulkas', 'Bekas', '2020-08-11'),
('r4342423', '002', 'Anggrek Office', '113', 'sukoco', '', 'Kulkas', 'bekas', '2020-08-04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemutihan`
--

CREATE TABLE `pemutihan` (
  `kode_pemutihan` varchar(20) NOT NULL,
  `kode_aset` varchar(20) NOT NULL,
  `nama_aset` varchar(100) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `lokasi` varchar(20) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `id_karyawan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(20) NOT NULL,
  `jabatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pemutihan`
--

INSERT INTO `pemutihan` (`kode_pemutihan`, `kode_aset`, `nama_aset`, `kondisi`, `lokasi`, `keterangan`, `id_karyawan`, `nama_karyawan`, `jabatan`) VALUES
('11231', '71314139996', 'BASE STATION XGE', 'RS', 'IT Tower', 'Baru dibeli', '211', 'Joko susilo', 'Manager Operasional'),
('121121121', '71310196285', 'BKS OUT STATIO XFAR    SCADA  ', 'HD', 'Pekanbaru', 'Masih hutang', 'Ucok', 'SBY', 'Teknisi'),
('121211', '71316139472', 'RADIO MOBILE XGE MC66KHS', 'HD', 'Duri', 'Rental', 'Masmur', 'Joko susilo', 'Manager Operasional'),
('212617298', '71316139472', 'RADIO MOBILE XGE MC66KHS', 'HD', 'Pekanbaru', 'Masih hutang', 'Ucok', 'Bambang Setiawan', 'Teknisi'),
('473920', 'WUWEURE', 'IDEPRGI', 'DWJPFI', 'Pekanbaru', 'Masih hutang', 'udin', 'Joko susilo', 'Manager'),
('888888', '71260176259', 'AMPLIFIER TBA XHITA 068242', 'BR', 'Sanhok', 'Banyak begal', '335', 'SBY', 'Teknisi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE `ruangan` (
  `kode_ruangan` varchar(20) NOT NULL,
  `noruang` varchar(20) NOT NULL,
  `nama_ruangan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(20) NOT NULL,
  `lokasi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ruangan`
--

INSERT INTO `ruangan` (`kode_ruangan`, `noruang`, `nama_ruangan`, `nama_karyawan`, `lokasi`) VALUES
('001', '23', 'IT Room', 'Bambang Setiawan', 'IT Tower Dumai'),
('002', '24', 'Anggrek Office', 'Reza Arab oktovian', 'Rumbai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`) VALUES
(0, 'alfi', 'gy8YWujW9ZuK459ILFLDcR0sD7WAgu31', '$2y$13$24HkXtCKz0XzHOZeygMO/Ohr.SIQmph05FlHWZaHVuZS5kYf3KyKG'),
(2, 'admin', '9XIiWajD9KkKdKpUb290pNU4nWOSH5w-', '$2y$13$/T04.KGKwa3wMqhbmFYd/uXc2RHV7Wpfweqg3bAGFgvbOjWJfIfVG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user2`
--

CREATE TABLE `user2` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user2`
--

INSERT INTO `user2` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `level`) VALUES
(1, 'bambang', 'IxlpnPbuLPYyi1md_TcyU8aPqo2wIu-G', '$2y$13$PbyOLl7mTYyglhbFga23Z.E42U1ucLkPeHMc1zCLS1f2zkWinfR4m', NULL, 'bambang@gmail.com', 10, NULL, NULL, 1),
(2, 'alfi', 'lBK2H7ytZ_ny41lRXrY81nsfFsrfN3u2', '$2y$13$HW3znfuuicCE8YMg/TTGueicYtgCwGM6ncqXiSYL2uZpBpintvm9C', NULL, '', 10, '0000-00-00', '0000-00-00', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aset`
--
ALTER TABLE `aset`
  ADD PRIMARY KEY (`kode_aset`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indeks untuk tabel `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`kode_mutasi`);

--
-- Indeks untuk tabel `pemutihan`
--
ALTER TABLE `pemutihan`
  ADD PRIMARY KEY (`kode_pemutihan`);

--
-- Indeks untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`kode_ruangan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user2`
--
ALTER TABLE `user2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user2`
--
ALTER TABLE `user2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
