<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ruangan;

/**
 * RuanganSearch represents the model behind the search form of `app\models\ruangan`.
 */
class RuanganSearch extends ruangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_ruangan'], 'safe'],
            [['nama_ruangan', 'nama_karyawan', 'lokasi'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ruangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nama_ruangan' => $this->nama_ruangan,
            'nama_karyawan' => $this->nama_karyawan,
            'lokasi' => $this->lokasi,
        ]);

        $query->andFilterWhere(['like', 'kode_ruangan', $this->kode_ruangan]);

        return $dataProvider;
    }
}
