<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\mutasi;

/**
 * MutasiSearch represents the model behind the search form of `app\models\mutasi`.
 */
class MutasiSearch extends mutasi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_mutasi', 'kode_ruangan', 'nama_ruangan', 'id_karyawan', 'nama_karyawan', 'nama_aset', 'kondisi', 'tgl_diterima'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = mutasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tgl_diterima' => $this->tgl_diterima,
        ]);

        $query->andFilterWhere(['like', 'kode_mutasi', $this->kode_mutasi])
            ->andFilterWhere(['like', 'kode_ruangan', $this->kode_ruangan])
            ->andFilterWhere(['like', 'nama_ruangan', $this->nama_ruangan])
            // ->andFilterWhere(['like', 'id_karyawan', $this->id_karyawan])
            // ->andFilterWhere(['like', 'nama_karyawan', $this->nama_karyawan])
            ->andFilterWhere(['like', 'nama_aset', $this->nama_aset])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi]);

        return $dataProvider;
    }
}
