<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\aset;

/**
 * AsetSearch represents the model behind the search form of `app\models\aset`.
 */
class AsetSearch extends aset
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_aset', 'nama_aset', 'sn_aset','lokasi','keterangan', 'kondisi'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = aset::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'kode_aset', $this->kode_aset])
            ->andFilterWhere(['like', 'nama_aset', $this->nama_aset])
            ->andFilterWhere(['like', 'sn_aset', $this->sn_aset])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi]);

        return $dataProvider;
    }
}
