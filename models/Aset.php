<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aset".
 *
 * @property string $kode_aset
 * @property string $nama_aset
 * @property string $sn_aset
 * @property string $kondisi
 */
class Aset extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aset';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_aset', 'nama_aset', 'sn_aset','lokasi','keterangan', 'kondisi'], 'required'],
            [['kode_aset','lokasi','keterangan'], 'string', 'max' => 20],
            [['nama_aset', 'sn_aset', 'kondisi'], 'string', 'max' => 50],
            [['kode_aset'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_aset' => 'Kode Aset',
            'nama_aset' => 'Nama Aset',
            'sn_aset' => 'Serial Number',
            'lokasi' => 'Lokasi',
            'keterangan' => 'Keterangan',
            'kondisi' => 'Kondisi',
        ];
    }
}
