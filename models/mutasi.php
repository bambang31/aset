<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mutasi".
 *
 * @property string $kode_mutasi
 * @property string $kode_ruangan
 * @property string $nama_ruangan
 * @property string $nama_aset
 * @property string $kondisi
 * @property string $tgl_diterima
 */
class mutasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mutasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_mutasi', 'kode_ruangan', 'nama_ruangan','kode_aset', 'nama_aset', 'kondisi', 'tgl_diterima'], 'required'],
            [['tgl_diterima'], 'safe'],
            [['kode_mutasi'], 'string', 'max' => 11],
            [['kode_ruangan', 'nama_ruangan', 'nama_aset'], 'string', 'max' => 50],
            [['kode_aset', 'kondisi'], 'string', 'max' => 20],
            [['kode_mutasi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_mutasi' => 'Kode Mutasi',
            'kode_ruangan' => 'Kode Ruangan',
            'nama_ruangan' => 'Nama Ruangan',
            // 'id_karyawan' => 'Id Karyawan',
            // 'nama_karyawan' => 'Nama Karyawan',
            'kode_aset' => 'kode Aset',
            'nama_aset' => 'Nama Aset',
            'kondisi' => 'Kondisi',
            'tgl_diterima' => 'Tgl Diterima',
        ];
    }
}
