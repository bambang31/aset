<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\pemutihan;

/**
 * PemutihanSearch represents the model behind the search form of `app\models\pemutihan`.
 */
class PemutihanSearch extends pemutihan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_pemutihan', 'kode_aset', 'nama_aset', 'kondisi', 'lokasi', 'keterangan', 'id_karyawan', 'nama_karyawan', 'jabatan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = pemutihan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'kode_pemutihan', $this->kode_pemutihan])
            ->andFilterWhere(['like', 'kode_aset', $this->kode_aset])
            ->andFilterWhere(['like', 'nama_aset', $this->nama_aset])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'id_karyawan', $this->id_karyawan])
            ->andFilterWhere(['like', 'nama_karyawan', $this->nama_karyawan])
            ->andFilterWhere(['like', 'jabatan', $this->jabatan]);

        return $dataProvider;
    }
}
