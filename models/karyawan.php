<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "karyawan".
 *
 * @property string $id_karyawan
 * @property string $nama_karyawan
 * @property string $jabatan
 * @property string $alamat
 */
class karyawan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'karyawan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_karyawan', 'nama_karyawan', 'jabatan', 'alamat'], 'required'],
            [['id_karyawan'], 'string', 'max' => 20],
            [['nama_karyawan', 'jabatan'], 'string', 'max' => 50],
            [['alamat'], 'string', 'max' => 100],
            [['id_karyawan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_karyawan' => 'Id Karyawan',
            'nama_karyawan' => 'Nama Karyawan',
            'jabatan' => 'Jabatan',
            'alamat' => 'Alamat',
        ];
    }
}
