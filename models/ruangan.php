<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruangan".
 *
 * @property string $kode_ruangan
 * @property int $nama_ruangan
 * @property int $nama_karyawan
 * @property int $lokasi
 */
class ruangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_ruangan', 'nama_ruangan', 'nama_karyawan', 'lokasi'], 'required'],
            [['nama_ruangan', 'nama_karyawan', 'lokasi'], 'string'],
            [['kode_ruangan'], 'string', 'max' => 20],
            [['kode_ruangan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_ruangan' => 'Kode Ruangan',
            'nama_ruangan' => 'Nama Ruangan',
            'nama_karyawan' => 'Nama Karyawan',
            'lokasi' => 'Lokasi',
        ];
    }
}
