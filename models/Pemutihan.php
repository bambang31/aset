<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemutihan".
 *
 * @property string $kode_pemutihan
 * @property string $kode_aset
 * @property string $nama_aset
 * @property string $kondisi
 * @property string $lokasi
 * @property string $keterangan
 * @property string $id_karyawan
 * @property string $nama_karyawan
 * @property string $jabatan
 */
class Pemutihan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pemutihan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_pemutihan', 'kode_aset', 'nama_aset', 'kondisi', 'lokasi', 'keterangan', 'id_karyawan', 'nama_karyawan', 'jabatan'], 'required'],
            [['kode_aset', 'kondisi', 'lokasi', 'id_karyawan', 'nama_karyawan', 'jabatan'], 'string', 'max' => 20],
            [['keterangan'], 'string', 'max' => 50],
            [['nama_aset'],'string', 'max' =>100], 
            [['kode_pemutihan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kode_pemutihan' => 'Kode Pemutihan',
            'kode_aset' => 'Kode Aset',
            'nama_aset' => 'Nama Aset',
            'kondisi' => 'Kondisi',
            'lokasi' => 'Lokasi',
            'keterangan' => 'Keterangan',
            'id_karyawan' => 'ID Karyawan',
            'nama_karyawan' => 'Nama Karyawan',
            'jabatan' => 'Jabatan',
        ];
    }
}
