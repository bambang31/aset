<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jabatan".
 *
 * @property string $id_jabatan
 * @property string $n_jabatan
 */
class Jabatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jabatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jabatan', 'n_jabatan'], 'required'],
            [['id_jabatan'], 'string', 'max' => 20],
            [['n_jabatan'], 'string', 'max' => 50],
            [['id_jabatan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_jabatan' => 'Id Jabatan',
            'n_jabatan' => 'Jabatan',
        ];
    }
}
